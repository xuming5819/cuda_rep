//
// Created by xuming on 2022/5/18.
//

#include <iomanip>
#include <iostream>
#include <stdio.h>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include "common.h"

using namespace std;


#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


//使用一维数组相加
__global__ void add(double *a, double *b, double *c, int N){
    //int tid = threadIdx.x;      //线程索引，启用1个线程块，每个线程块N个线程
    int tid = blockIdx.x *blockDim.x + threadIdx.x;

    printf("----------->%d %d %d %d\n", tid, blockIdx.x, blockDim.x, threadIdx.x);
    if (tid < N){
        c[tid] = a[tid] + b[tid];
    }
}


void InitOneDimArray(double *a, double d, int N){
    for (int i = 0; i < N; i++){
        a[i] = (i+1) * d;
    }
}

int main()
{
    // 检测 GPU
    if (!CheckCUDA())
    {
        std::cout << "No CUDA deveice ..." << std::endl;
        return 0;
    }
    // 基于GPU的矢量求和

    int N = 20;                  //定义数组大小
    double *h_a, *h_b, *h_c;      //声明在CPU上使用的指针
    double *dev_a, *dev_b, *dev_c;             //声明在GPU上使用的指针
    //为数组分配大小
    h_a = new double[N];
    h_b = new double[N];
    h_c = new double[N];

    // 在 GPU 分配内存

    HANDLE_ERROR(cudaMalloc( (void**)&dev_a, N*sizeof(double)) );
    HANDLE_ERROR(cudaMalloc( (void**)&dev_b, N*sizeof(double)) );
    HANDLE_ERROR(cudaMalloc( (void**)&dev_c, N*sizeof(double)) );

    // 在 CPU 上为数组 `a` 和 `b` 赋值

    InitOneDimArray(h_a, 1.1, N);
    InitOneDimArray(h_b, 2.2, N);

    // 将数组 `a` 和 `b` 赋值到　GPU 中

    HANDLE_ERROR(cudaMemcpy(dev_a, h_a, N*sizeof(double), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_b, h_b, N*sizeof(double), cudaMemcpyHostToDevice));

    add<<<2,N>>>(dev_a, dev_b, dev_c, N);

    // 将数组 `c` 从GPU赋值到CPU


    HANDLE_ERROR(cudaMemcpy(h_c, dev_c, N*sizeof(double), cudaMemcpyDeviceToHost));

    // 显示结果

    for (int i=0;i<N;i++){
        //printf("%d + %d = %d\n", h_a[i], h_b[i], h_c[i]);
        std::cout <<  h_a[i] << " + " << h_b[i] << " = " << h_c[i] << endl;
    }

    // 在 GPU 上分配内存


    // 释放在 GPU 上分配的内存

    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);


    return 0;
}

