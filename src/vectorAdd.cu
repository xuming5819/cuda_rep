//
// Created by xuming on 2022/5/19.
//

/*!
 * 向量加法:C = A + B
 */

#include <iostream>
#include <cuda_runtime.h>

#include "common.h"

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

__global__ void vectorAdd(const float *A, const float *B, float *C, int NSize)
{
    // Grid 一维, block 1 维, thread 一维
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid < NSize)
    {
        C[tid] = A[tid] + B[tid];
    }
}

int main()
{
    // 检测 GPU
    if (!CheckCUDA()){
        std::cout << "No CUDA deveice ..." << std::endl;
        return 0;
    }

    // 设置数组长度
    int N = 500000;
    std::cout << "[Vector addition of " << N << " elements]" << std::endl;

    // 在 host 上分配内存

    float *hos_A, *hos_B, *hos_C;      //声明在CPU上使用的指针
    float *dev_A, *dev_B, *dev_C;      //声明在GPU上使用的指针
    //为数组分配大小
    hos_A = new float[N];
    hos_B = new float[N];
    hos_C = new float[N];

    // 验证分配内存成功
    if (hos_A == nullptr || hos_B == nullptr || hos_C == nullptr){
        std::cout << stderr << " Failed to allocate host vector!" << std::endl;
        exit(EXIT_FAILURE);
    }

    // 初始化 host 输入的向量
    // 随机初始化向量的值
    for (int i=0;i<N;i++){
        hos_A[i] = rand() / (float)RAND_MAX;
        hos_B[i] = rand() / (float)RAND_MAX;
    }

    // 在 GPU 上开辟内存
    HANDLE_ERROR(cudaMalloc( (void**)&dev_A, N*sizeof(float)) );
    HANDLE_ERROR(cudaMalloc( (void**)&dev_B, N*sizeof(float)) );
    HANDLE_ERROR(cudaMalloc( (void**)&dev_C, N*sizeof(float)) );


    // Launch the vector Add CUDA kernel

    int threadsPerBlock = 256;
    int blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
    HANDLE_ERROR(cudaMemcpy(dev_A, hos_A, N*sizeof(float), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_B, hos_B, N*sizeof(float), cudaMemcpyHostToDevice));
    vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(dev_A, dev_B, dev_C, N);

    HANDLE_ERROR(cudaMemcpy(hos_C, dev_C, N*sizeof(float), cudaMemcpyDeviceToHost));

    // Verify that the result vector is correct
    for (int i = 0; i < N; i++)
    {
        if (fabs(hos_A[i] + hos_B[i] - hos_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            exit(EXIT_FAILURE);
        }
    }
    printf("Test PASSED\n");
    // 释放在 GPU 上分配的内存
    cudaFree(dev_A);
    cudaFree(dev_B);
    cudaFree(dev_C);

    // 在 CPU 上释放内存
    delete[] hos_A;
    delete[] hos_B;
    delete[] hos_C;

    return 0;



}