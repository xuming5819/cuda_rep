//
// Created by xuming on 2022/7/11.
//

#include <iostream>

// 简单工厂

class AbstractFruit{
public:
    virtual void ShowName() = 0;
};


class Apple: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是苹果 ..." << std::endl;
    }
};

class Banana: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是香蕉..." << std::endl;
    }
};

class Pear: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是鸭梨..." << std::endl;
    }
};

// 写工厂
class FruitFactory{
public:
    static AbstractFruit* CreateFruit(std::string flag){
        if (flag == "apple"){
            return new Apple;
        }else if (flag == "banana"){
            return new Banana;
        }else if (flag == "pear"){
            return new Pear;
        } else{
            return nullptr;
        }
    }
};


void test01()
{
    FruitFactory* factory = new FruitFactory;
    AbstractFruit* apple = factory->CreateFruit("apple");
    apple->ShowName();
    AbstractFruit* banana = factory->CreateFruit("banana");
    banana->ShowName();
    AbstractFruit* pear = factory->CreateFruit("pear");
    pear->ShowName();


}

int main()
{
    test01();
    return 0;
}

