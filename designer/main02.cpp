//
// Created by xuming on 2022/7/10.
//

#include <iostream>
#include <cstring>
#include <vector>
// 迪米特法则案例, 最少知识原则


class AbstricBuilding{
public:
    virtual void sale() = 0;
    virtual std::string getQuality() = 0;
};


class BuildingA: public AbstricBuilding{
public:
    BuildingA(){
        m_Quality = "高品质";
    }
    virtual void sale()
    {
        std::cout << "楼盘 A " << m_Quality << "被售卖 ..." << std::endl;
    }

    virtual std::string getQuality(){
        return m_Quality;
    }

public:
    std::string m_Quality;

};


class BuildingB: public AbstricBuilding{
public:
    BuildingB(){
        m_Quality = "低品质";
    }
    virtual void sale()
    {
        std::cout << "楼盘 B " << m_Quality << "被售卖 ..." << std::endl;
    }

    virtual std::string getQuality(){
        return m_Quality;
    }

public:
    std::string m_Quality;

};

// 中介类

class Mediator{
public:
    Mediator(){
        AbstricBuilding* building = new BuildingA;
        vBuilding.push_back(building);
        building = new BuildingB();
        vBuilding.push_back(building);
    }


    ~Mediator(){
        for (std::vector<AbstricBuilding*>::iterator it = vBuilding.begin();it!=vBuilding.end();it++){
            if((*it) != nullptr){
                delete *it;
            }
        }
    }

    // 对外提供接口
    AbstricBuilding* findBuilding(std::string quality){
        for (std::vector<AbstricBuilding*>::iterator it = vBuilding.begin();it!=vBuilding.end();it++) {
            if ((*it)->getQuality() == quality){
                return *it;
            }
        }
        return nullptr;
    }

public:
    std::vector<AbstricBuilding*> vBuilding;
};

void test01()
{
    BuildingA* ba = new BuildingA;


    if (ba->m_Quality == "高品质"){
        ba->sale();
    }
    BuildingB* bb = new BuildingB;
    if (bb->m_Quality == "低品质"){
        bb->sale();
    }

}

void test02()
{
    Mediator* mediator = new Mediator;
    AbstricBuilding* building = mediator->findBuilding("高品质");
    if (building!= nullptr){
        building->sale();
    }
    else{
        std::cout << "没有符合您条件的楼盘 ..." << std::endl;
    }
}

int main()
{
    test01();
    test02();
    return 0;
}