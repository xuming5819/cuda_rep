//
// Created by xuming on 2022/7/18.
//

#include <iostream>
#include <vector>
#include <algorithm>

// 这个函数我已经写好了
struct MyPrint{
    void operator() (int v1, int v2)
    {
        std::cout << "v1 + v2 = " << v1 + v2 << std::endl;
    }
};


// 目标接口, 我要转换适配

class Target{
public:
    virtual void operator() (int v) = 0;
};

// 写适配器

class Adapter: public Target{
public:
    void operator() (int v){
        print(v, 100);
    }

public:
    MyPrint print;
};


int main()
{
    std::vector<int> v;
    for(int i=0; i<10; i++){
        v.push_back(i);
    }

    std::for_each(v.begin(),v.end(),Adapter());
    return 0;
}
