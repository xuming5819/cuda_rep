//
// Created by xuming on 2022/7/12.
//

// 单例模式

#include <iostream>

class A{
private:
    A(){
        a = new A;
    }

public:
    static A* getInstance()
    {
        return a;
    }

private:
    static A* a;
};


A* A::a = nullptr;

#if 0
// 饿汉式

class MyClass {
public:
    static MyClass *getInstance() {
        std::cout << "+++++++++++++++++" << std::endl;
        return m_instance;
    }

    static void destory() {
        if (m_instance) {
            delete m_instance;
            m_instance = nullptr;
        }
    }

private:
    static MyClass *m_instance;

};

MyClass* MyClass::m_instance = nullptr; //定义在.cpp中


int main()
{
   MyClass::getInstance();
    return 0;
}

// 实现单例的步骤
/*
 * 构造函数私有化
 * 增加静态私有的当前类的指针变量
 * 提供一个静态对外的接口，可以让用户获得单例对象
 */
# endif

//MyClass.h

class MyClass{
public:
    static MyClass* getInstance(){
        if (m_instance == nullptr) {
            m_instance = new MyClass();
        }
        return m_instance;
    }
private:
    MyClass() {} //私有构造函数
    MyClass(const MyClass& my);//禁止拷贝构造函数
    MyClass& operator=(const MyClass &my); //禁止赋值操作
    static MyClass* m_instance;
};

//MyClass.cpp
MyClass* MyClass::m_instance = nullptr;// 存放在Cpp中，若放在.h中，.h被包含多次时重复定义
