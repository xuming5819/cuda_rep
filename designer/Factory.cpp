//
// Created by xuming on 2022/7/11.
//


#include <iostream>

// 简单工厂

class AbstractFruit{
public:
    virtual void ShowName() = 0;
};


class Apple: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是苹果 ..." << std::endl;
    }
};

class Banana: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是香蕉..." << std::endl;
    }
};

class Pear: public AbstractFruit{
public:
    virtual void ShowName(){
        std::cout << "我是鸭梨..." << std::endl;
    }
};


// 抽象工厂

class AbstractFruitFactory{
public:
    virtual AbstractFruit* CreateFruit() = 0;
};

// 苹果工厂

class AppleFactory: public AbstractFruitFactory{
    virtual AbstractFruit* CreateFruit(){
        return new Apple;
    }
};

class BananaFactory: public AbstractFruitFactory{
    virtual AbstractFruit* CreateFruit(){
        return new Banana;
    }
};

class PearFactory: public AbstractFruitFactory{
    virtual AbstractFruit* CreateFruit(){
        return new Pear;
    }
};




void test01()
{
   AbstractFruitFactory* factory = nullptr;
   AbstractFruit* fruit = nullptr;
   // 创建一个苹果工厂
   factory = new AppleFactory;
   fruit = factory->CreateFruit();
   fruit->ShowName();

   delete fruit;
   delete factory;






}

int main()
{
    test01();
    return 0;
}



