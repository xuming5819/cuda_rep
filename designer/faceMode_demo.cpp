//
// Created by xuming on 2022/7/17.
//

#include <iostream>

class Television{
public:
    void On()
    {
        std::cout << "TV Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "TV Close ......" << std::endl;
    }
};




class Light{
public:
    void On()
    {
        std::cout << "Light Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "Light Close ......" << std::endl;
    }
};

class Audio{
public:
    void On()
    {
        std::cout << "Audio Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "Audio Close ......" << std::endl;
    }
};


class Mircophone{
public:
    void On()
    {
        std::cout << "Mircophone Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "Mircophone Close ......" << std::endl;
    }
};

class DVDPlayer{
public:
    void On()
    {
        std::cout << "DVDPlayer Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "DVDPlayer Close ......" << std::endl;
    }
};

class GameMachine{
public:
    void On()
    {
        std::cout << "GameMeachine Open ......" << std::endl;
    }

    void Off()
    {
        std::cout << "GameMeachine Close ......" << std::endl;
    }
};

// 提供两个外观

class KTVMode{
public:
    KTVMode(){
        pTv = new Television;
        pLight = new Light;
        pAudio = new Audio;
        pMircophone = new Mircophone;
        pDVDPlayer = new DVDPlayer;
    }
    ~KTVMode()
    {
        delete pTv;
        delete pLight;
        delete pAudio;
        delete pDVDPlayer;
        delete pMircophone;

    }

    void OnKTV()
    {
        pTv->On();
        pLight->On();
        pAudio->On();
        pMircophone->On();
        pDVDPlayer->On();
    }

    void OffKTV()
    {
        pTv->Off();
        pLight->Off();
        pAudio->Off();
        pMircophone->Off();
        pDVDPlayer->Off();
    }

public:
    Television* pTv;
    Light* pLight;
    Audio* pAudio;
    Mircophone* pMircophone;
    DVDPlayer* pDVDPlayer;

};


class XXXMode{
public:
    XXXMode()
    {
        pGameMachine = new GameMachine;
    }



public:
    GameMachine* pGameMachine;
};


void test01()
{
    KTVMode* ktv = new KTVMode;
    ktv->OnKTV();
    ktv->OffKTV();
}
int main()
{
    test01();
    return 0;
}