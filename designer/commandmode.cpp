//
// Created by xuming on 2022/7/22.
//

#include <iostream>
#include <queue>
#include <unistd.h>  // 睡眠函数
// 命令模式


class HandleClientProtocol{
public:
    // 处理增加金币
    void AddMoney(){
        std::cout << "给玩家增加金币! ..." << std::endl;
    }

    // 处理增加钻石
    void AddDiamond(){
        std::cout << "给玩家增加钻石 ..." << std::endl;
    }

    // 处理玩家装备
    void AddEquipment(){
        std::cout << "给玩家穿装备 ..." << std::endl;
    }

    // 处理玩家升级
    void addLevel(){
        std::cout << "给玩家升级 ..." << std::endl;
    }

};

// 协议命令接口
class AbstractCommand{
public:
    virtual void handle() = 0; // 处理客户端请求的接口
};


class AddMoneyCommand: public AbstractCommand{
public:

    AddMoneyCommand(HandleClientProtocol* protocol){
        this->pProtocol = protocol;
    }

    virtual void handle(){
        this->pProtocol->AddMoney();
    }

public:
    HandleClientProtocol* pProtocol;
};

// 处理钻石的请求

class AddDiamondCommand: public AbstractCommand{
public:

    AddDiamondCommand(HandleClientProtocol* protocol){
        this->pProtocol = protocol;
    }

    virtual void handle(){
        this->pProtocol->AddDiamond();
    }

public:
    HandleClientProtocol* pProtocol;
};

// 处理装备请求
class AddEquipmentCommand: public AbstractCommand{
public:

    AddEquipmentCommand(HandleClientProtocol* protocol){
        this->pProtocol = protocol;
    }

    virtual void handle(){
        this->pProtocol->AddEquipment();
    }

public:
    HandleClientProtocol* pProtocol;
};

// 处理升级请求
class AddLevelCommand: public AbstractCommand{
public:

    AddLevelCommand(HandleClientProtocol* protocol){
        this->pProtocol = protocol;
    }

    virtual void handle(){
        this->pProtocol->addLevel();
    }

public:
    HandleClientProtocol* pProtocol;
};




// 服务器程序
class Server{
    // 让玩家处理协议排队
public:
    void AddRequest(AbstractCommand* command){
        m_Commands.push(command);
    }

    void startHandle(){
        // 开始处理
        while (!m_Commands.empty()){
            usleep(100000);
            AbstractCommand* command = m_Commands.front();
            command->handle();
            m_Commands.pop();
        }
    }
public:
    std::queue<AbstractCommand*> m_Commands;


};

void test01()
{
    HandleClientProtocol* protocol = new HandleClientProtocol;
    AbstractCommand* addmoney = new AddMoneyCommand(protocol);

    AbstractCommand* addDiamond = new AddDiamondCommand(protocol);

    AbstractCommand* addEquipment = new AddEquipmentCommand(protocol);

    // 客户端 升级请求
    AbstractCommand* addLevel = new AddLevelCommand(protocol);

    Server* server = new Server;
    server->AddRequest(addmoney);

    server->AddRequest(addDiamond);

    server->AddRequest(addEquipment);

    server->AddRequest(addLevel);

    // 服务器开始请求
    server->startHandle();

}

int main()
{
    test01();

    return 0;
}