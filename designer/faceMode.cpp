//
// Created by xuming on 2022/7/17.
//

#include <iostream>
/*
 * 外观模式就是将复杂的子系统类抽象到统一个的接口进行管理，外界只需要
 * 通过此接口于子类系统进行交互，而不必直接参与复杂的子类进行交互
 */

class SubSystem1{
public:
    void run(){
        std::cout << "子系统 1 .... " << std::endl;
    }
};


class SubSystem2{
public:
    void run(){
        std::cout << "子系统 2 .... " << std::endl;
    }
};


class SubSystem3{
public:
    void run(){
        std::cout << "子系统 3 .... " << std::endl;
    }
};


class SubSystem4{
public:
    void run(){
        std::cout << "子系统 4 .... " << std::endl;
    }
};


class FaceMode{
public:
    FaceMode(){
        pSystem1 = new SubSystem1;
        pSystem2 = new SubSystem2;
        pSystem3 = new SubSystem3;
        pSystem4 = new SubSystem4;
    }

    void run()
    {
        pSystem1->run();
        pSystem2->run();
        pSystem3->run();
        pSystem4->run();
    }
    ~FaceMode()
    {
        delete pSystem4;
        delete pSystem3;
        delete pSystem2;
        delete pSystem1;
    }


public:
    SubSystem1* pSystem1;
    SubSystem2* pSystem2;
    SubSystem3* pSystem3;
    SubSystem4* pSystem4;

};

void test01()
{
    FaceMode* pSystem = new FaceMode;
    pSystem->run();

    delete pSystem;
}

int main()
{
    test01();
    return 0;
}