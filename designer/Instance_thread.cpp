//
// Created by xuming on 2022/7/13.
//

// 单例碰到多线程

// 懒汉式

class Singleton_lazy{
private:
    Singleton_lazy(){}

public:
    static Singleton_lazy* getInstance()
    {
        if (pSingleton = nullptr)
        {
            pSingleton = new Singleton_lazy;
        }
        return pSingleton;
    }

private:
    static Singleton_lazy* pSingleton;
};

Singleton_lazy* Singleton_lazy::pSingleton = nullptr;

// 饿汉式

class Singleton_hungry{
private:
    Singleton_hungry(){}

public:
    static Singleton_hungry* getInstance()
    {
        return pSingleton;
    }

private:
    static Singleton_hungry* pSingleton;
};

Singleton_hungry* Singleton_hungry::pSingleton = new Singleton_hungry;

// 懒汉式碰到多线程， 线程不安全， 加锁，解锁
// 饿汉式是线程安全



