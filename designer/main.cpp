#include <iostream>



// 依赖关系
// --------------------------------------------
class BMW{
public:
    void run(){
        std::cout << "别摸我启动 ... 快乐去上班" << std::endl;
    }
};

// 开车去上班
 // ------------------------------------------
class Person{
public:
    void GoWalk(BMW* car){
        car->run();

    }

};

// 关联关系
// ------------------------------------------
class Person2{
public:
    void GoWalk(){

    }

public:
    BMW* car; // 车作为人的一部分
};

// 聚合关系, 整体和部分的关系
// 电脑  <------- CPU / 硬盘
// ------------------------------------------

// 组合关系
// 公司 <------ 部门
// ------------------------------------------








int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}


