//
// Created by xuming on 2022/7/12.
//

#include <iostream>


// 抽象工厂

// 针对的是产品族，而不是产品的等级结构

// 抽象苹果
class AbstractApple{
public:
    virtual void ShowName() = 0;
};

class ChinaApple: public AbstractApple{
public:
    virtual void ShowName() {
        std::cout << "中国苹果 ..." << std::endl;

    }
};


class USAApple: public AbstractApple{
public:
    virtual void ShowName() {
        std::cout << "美国苹果 ..." << std::endl;

    }
};


class JapanApple: public AbstractApple{
public:
    virtual void ShowName() {
        std::cout << "日本苹果 ..." << std::endl;

    }
};

class AbstractBanana{
public:
    virtual void ShowName() = 0;
};

class ChinaBanana: public AbstractBanana{
public:
    virtual void ShowName() {
        std::cout << "中国香蕉 ..." << std::endl;

    }
};


class USABanana: public AbstractBanana{
public:
    virtual void ShowName() {
        std::cout << "美国香蕉  ..." << std::endl;

    }
};


class JapanBanana: public AbstractBanana{
public:
    virtual void ShowName() {
        std::cout << "日本香蕉 ..." << std::endl;

    }
};

class AbstractFactory{
public:
    virtual AbstractApple* CreateApple() = 0;
    virtual AbstractBanana* CreateBanana() = 0;
};



// 中国工厂

class ChinaFactory: public AbstractFactory{
    virtual AbstractApple* CreateApple(){
        return new ChinaApple;
    }

    virtual AbstractBanana* CreateBanana(){
        return new ChinaBanana;
    }
};

// 美国工厂

class USAFactory: public AbstractFactory{
    virtual AbstractApple* CreateApple(){
        return new USAApple;
    }

    virtual AbstractBanana* CreateBanana(){
        return new USABanana;
    }
};

// 日本工厂

class JapanFactory: public AbstractFactory{
    virtual AbstractApple* CreateApple(){
        return new JapanApple;
    }

    virtual AbstractBanana* CreateBanana(){
        return new JapanBanana;
    }
};

void test01()
{
    AbstractFactory* factory = nullptr;
    AbstractApple* apple = nullptr;
    AbstractBanana* banana = nullptr;

    factory = new ChinaFactory;
    apple = factory->CreateApple();
    banana = factory->CreateBanana();
    apple->ShowName();
    banana->ShowName();

//    factory = new USAFactory;
//    factory->CreateApple();
//    factory->CreateBanana();

    delete banana;
    delete apple;
    delete factory;


}

int main()
{
    test01();
    return 0;

}