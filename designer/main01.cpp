//
// Created by xuming on 2022/7/9.
//


/*
 * 开闭原则, 对扩展开放，对修改关闭
 */
#include <iostream>

// 抽象类
class AbstrcatCaculator{
public:
    virtual int getResult() = 0;
    virtual void setOperatorNUmber(int a, int b) = 0;
};


// 加法类
class PlusCaculator: public AbstrcatCaculator{
public:

    virtual void setOperatorNUmber(int a, int b){
        this->m_A = a;
        this->m_B = b;
    }
    virtual int getResult(){
        return m_A + m_B;
    }

private:
    int m_A;
    int m_B;
};


// 减法类
class MinuteCaculator: public AbstrcatCaculator{
public:

    virtual void setOperatorNUmber(int a, int b){
        this->m_A = a;
        this->m_B = b;
    }
    virtual int getResult(){
        return m_A - m_B;
    }

private:
    int m_A;
    int m_B;
};


// 乘法类
class MultiplyCaculator: public AbstrcatCaculator{
public:

    virtual void setOperatorNUmber(int a, int b){
        this->m_A = a;
        this->m_B = b;
    }
    virtual int getResult(){
        return m_A * m_B;
    }

private:
    int m_A;
    int m_B;
};



int main()
{
    AbstrcatCaculator* caculator = new PlusCaculator;
    caculator->setOperatorNUmber(3, 4);
    std::cout << "ret:------> "<< caculator->getResult() << std::endl;
    return 0;
}

