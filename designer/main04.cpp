//
// Created by xuming on 2022/7/11.
//

#include <iostream>

// 依赖倒装原则


class BankWorker{
public:
    void saveService(){
        std::cout << "办理存款业务 ..." << std::endl;
    }

    void payService(){
        std::cout << "办理支付业务 ..." << std::endl;
    }

    void tranferService(){
        std::cout << "办理转账业务 ..." << std::endl;
    }
};

// 中层模块
void doSaveBussiness(BankWorker* worker){
    worker->saveService();
}
void doPayussiness(BankWorker* worker){
    worker->payService();
}

void doTranferBussiness(BankWorker* worker){
    worker->tranferService();
}


// 业务模块

void test01(){
    BankWorker* worker = new BankWorker;
    doSaveBussiness(worker); // 办理存款业务
    doPayussiness(worker);
    doTranferBussiness(worker);
};


class AbstractWorker{
public:
    virtual void doBusiness() = 0;
};

class SaveBankWorker: public AbstractWorker{
public:
    virtual void doBusiness(){
        std::cout << "办理存款业务 ..." << std::endl;
    }
};

class PayBankWorker: public AbstractWorker{
public:
    virtual void doBusiness(){
        std::cout << "办理支付业务 ..." << std::endl;
    }
};


class TranferBankWOrker: public AbstractWorker{

public:
    virtual void doBusiness(){
        std::cout << "办理转账业务 ..." << std::endl;
    }
};

// 中层业务模块

void doNewBusiness(AbstractWorker* worker){
    worker->doBusiness();
    delete worker;
}


void test02(){

    doNewBusiness(new TranferBankWOrker);
    doNewBusiness(new PayBankWorker);
    doNewBusiness(new SaveBankWorker);

}

int main()
{
    //test01();
    test02();
    return 0;
}