//
// Created by xuming on 2022/7/21.
//


// 策略模式

// 抽象武器 武器策略
#include <iostream>

class WeaponStrategy{
public:
    virtual void UseWeapon() = 0;
};

class Knife : public WeaponStrategy
{
public:
    virtual void UseWeapon()
    {

        std::cout << "使用匕首 ..." << std::endl;
    }
};

class AK47: public WeaponStrategy{
public:
    virtual void UseWeapon()
    {
        std::cout << "使用 AK47  ..." << std::endl;
    }
};

class Character{
public:
    void setWeapon(WeaponStrategy* weapon){
        this->pWeapon = weapon;
    }

    void ThrowWeapon(){
        this->pWeapon->UseWeapon();
    }
public:
    WeaponStrategy* pWeapon;
};


void test01(){
    // 创建校色
    Character* character = new Character;


    // 武器策略

    WeaponStrategy* knife = new Knife;
    character->setWeapon(knife);   // 装上武器
    character->ThrowWeapon();      // 挥动武器



    WeaponStrategy* ak47 = new AK47;
    character->setWeapon(ak47);   // 装上武器
    character->ThrowWeapon();      // 挥动武器
};
int main()
{
    test01();
    return 0;
}
