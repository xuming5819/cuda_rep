from abc import ABCMeta, abstractmethod



class Payment(metaclass=ABCMeta):
    @abstractmethod
    def pay(self, money):
        pass


class AliPay(Payment):
    def pay(self, money):
        print("支付宝支付 %d 元" % money)


class WechatPay(Payment):
        def pay(self, money):
            print("微信支付 %d 元" % money)


# 抽象工厂角色
class PaymentFactory(metaclass=ABCMeta):
    @abstractmethod
    def create_payment(self):
        pass

# 具体工厂角色
class AliPayFactory(PaymentFactory):
    def create_payment(self):
        return  AliPay()


class WechatFactory(PaymentFactory):
    def create_payment(self):
        return WechatPay()


factory = AliPayFactory()
pay = factory.create_payment()
pay.pay(100)

"""
定义一个用于创建对象的接口，让子类决定实例化哪一个产品类
角色
抽象工厂 create
具体工行角色 concrete creator
抽象产品角色 product
具体产品角色 concrete product


优点
每个产品都对应一个具体的工厂类，不需要修改工厂类的代码
隐藏了对象创建的细节

缺点 每个增加一个具体产品类，就必须增加一个相应的具体工厂类
"""