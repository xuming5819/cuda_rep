



# class Payment:
#     def pay(self, money):
#         raise NotImplementedError


# # class Alipay:
# #     def pay(self, money):
# #         pass

# # class WechatPay:
# #     def pay(self, momeny):
# #         pass


# # 抽象方法

from abc import ABCMeta, abstractmethod
from curses.ascii import US


class Payment(metaclass=ABCMeta):
    @abstractmethod
    def pay(self, money):
        pass


class Alipay(Payment):
    def pay(self, money):
        print("支付宝支付 %d 元" % money)


class Wechatpay(Payment):
    def pay(self, money):
        print("微信支付 %d 元" % money)


p = Alipay()
p.pay(10)

"""
接口方便

开放封闭原则: 一个软件实体入类、模块和函数营该对扩展开放，对修改关闭，
里式替换原则: 所有引用父类的地方必须能透明地使用其子类的方法
依赖倒置原则: 高内聚低耦合，抽象不依赖细节，细节应该依赖抽象
接口隔离原则: 多个专一的接口 
单一职责原则: 


设计模式分类

创建型模式: 工厂方法模式，抽象工长模式，创建者模式，原型模式，单例模式

结构型模式: 适配器模式，桥模式，组合模式，装饰模式，外观模式，享元模式,代理模式

行为型模式: 解释器模式，责任链模式，命令模式，迭代器模式，中介者模式，备忘录模式，观察者模式，状态模式
          策略模式，访问者模式，模板方法

          



"""


class User:
    def show_name(self):
        pass


class VIPUser(User):
    def show_name(self):
        pass


