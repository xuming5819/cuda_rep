"""
桥模式


"""

from abc import ABCMeta, abstractmethod


# 抽象
class Shape(metaclass=ABCMeta):
    def __init__(self, color):
        self.color = color
    @abstractmethod
    def draw(self):
        pass

class Color(metaclass=ABCMeta):     
    @abstractmethod
    def paint(self, shape):
        pass

# 
class Rectangle(Shape):
    name = "矩形"
    def draw(self):
        self.color.paint(self)


class Circle(Shape):
    name = "圆形"
    def draw(self):
        self.color.paint(self)

class Red(Color):
    @classmethod
    def paint(self, shape):
        print("红色的 %s " % shape.name)
    
class Green(Color):
    @classmethod
    def paint(self, shape):
        print("绿色 %s " % shape.name)


shape = Rectangle(Red)
shape.draw()

"""
抽象和实现相分离
优秀的扩展能力
"""