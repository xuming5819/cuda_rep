"""
建造者模式
"""

from abc import ABC, ABCMeta, abstractmethod

# 角色
class Player:
    def __init__(self, face=None, body=None, arm=None, leg=None):
        self.face = face
        self.body = body
        self.arm = arm
        self.leg = leg

    def __str__(self):
        return "%s, %s, %s, %s" % (self.face, self.body, self.arm, self.leg)


# 抽象建造者
class Playerbuilder(metaclass=ABCMeta):
    @abstractmethod
    def build_face(self):
        pass


    @abstractmethod
    def build_body(self):
        pass


    @abstractmethod
    def build_arm(self):
        pass


    @abstractmethod
    def build_leg(self):
        pass

#  具体建造者
class SexGirlBuilder(Playerbuilder):
    def __init__(self):
        self.player = Player()

    def build_face(self):
        self.player.face = "漂亮脸蛋"

    def build_body(self):
        self.player.body = "苗条的身材"

    def build_arm(self):
        self.player.arm = "细瘦的胳膊"

    def build_leg(self):
        self.player.leg = "大长腿"



class Monster(Playerbuilder):
    def __init__(self):
        self.player = Player()

    def build_face(self):
        self.player.face = "怪兽脸"

    def build_body(self):
        self.player.body = "怪兽身材"

    def build_arm(self):
        self.player.arm = "胳膊"

    def build_leg(self):
        self.player.leg = "短腿"



# 具体建造者
class PlayerDirector:
    def build_player(self, builder):
        builder.build_body()
        builder.build_face()
        builder.build_arm()
        builder.build_leg()
        return builder.player


# 客户端调用

builder = SexGirlBuilder()  # 抽象建造者
director = PlayerDirector() # 指挥者
p = director.build_player(builder)  # 产品
print(p)

    
"""
将一个复杂对象的构建与它的表示分类，使得同样的构建过程可以创建不同的尔表示

角色:
    抽象建造者 (builder)
    具体建造者 (concrete builder)
    指挥者 (director)
    产品 (product)
"""