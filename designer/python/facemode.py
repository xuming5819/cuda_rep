


class CPU:
    def run(self):
        print("CPU 开始运行")

    def stop(self):
        print("CPU 停止运行")


class Disk:
    def run(self):
        print("Disk 开始运行")

    def stop(self):
        print("Disk 停止运行")

class Memory:
    def run(self):
        print("Memory 开始运行")

    def stop(self):
        print("Memory 停止运行")


# 外观
class Computer:
    def __init__(self):
        self.cpu = CPU()
        self.disk = Disk()
        self.memory = Memory()

    def run(self):
        self.cpu.run()
        self.disk.run()
        self.memory.run()


    def stop(self):
        self.cpu.stop()
        self.disk.stop()
        self.memory.stop()


computer = Computer()
computer.run()
computer.stop()
