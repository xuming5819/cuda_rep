"""
简单工厂
"""

from abc import ABCMeta, abstractmethod
from termios import CREAD

class Payment(metaclass=ABCMeta):
    @abstractmethod
    def pay(self, money):
        pass


class AliPay(Payment):
    def pay(self, money):
        print("支付宝支付 %d 元" % money)


class WechatPay(Payment):
        def pay(self, money):
            print("微信支付 %d 元" % money)




class PaymentFactory:
    def create_payment(self, method):
        if method == "alipay":
            return AliPay()
        elif method == "wechat":
            return WechatPay()
        else:
            raise TypeError("No such payment named %s" % method)


# 不直接向客户端暴露对象创建的实现细节，而是通过一个工厂类来负责创建产品类的实例

"""
角色:
    工厂角色 create
    抽象产品角色 product
    具体产品角色 concrete product


优点:
    隐藏了对向创建的细节
    客户端不需要修改代码

缺点:
    违反了单一职责原则，将创建逻辑集中到一个工厂类里
    当添加新产品时，需要修改工厂类代码，违反了开闭原则
"""



pay = PaymentFactory()
p = pay.create_payment("alipay")
p.pay(100)

p1 = pay.create_payment("wechat")
p1.pay(200)
