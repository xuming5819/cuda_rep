"""
适配器模式

将一个类的接口转换成客户希望的另外一个接口，适配模式使得原本由于接口不兼容而不能一起工作的那些类一起工作

两种适配器实现方式


类适配器，使用多继承
对象适配器, 使用组合

"""


from abc import ABCMeta, abstractmethod


class Payment(metaclass=ABCMeta):
    @abstractmethod
    def pay(self, money):
        pass


class AliPay(Payment):
    def pay(self, money):
        print("支付宝支付 %d 元." % money)


class WechatPay(Payment):
    def pay(self, money):
        print("微信支付 %d 元." % money)


#待适配的类
class BankPay:
    @classmethod
    def cost(self, money):
        print('银联支付 %d 元.' % money)

# class ApplePay:
#     def cost(self,money):
#         print('苹果支付%d元。' % money)

# #适配器
# #   类适配器（使用多继承）
# class NewBankPay(Payment,BankPay):
#     def pay(self,money):
#         self.cost(money)



# p = NewBankPay()
# p.pay(100)



"""
将两个不同的接口，保持一致，复用代码
"""

#   对象适配器（使用组合）
class PaymentAdapter(Payment):
    def __init__(self, payment):
        self.payment = payment

    def pay(self,money):
        self.payment.cost(money)

p = PaymentAdapter(BankPay)
p.pay(100)

