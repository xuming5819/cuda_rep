"""
单例模式

    内容: 保证一个类只有一个实例，并且提供一个访问它的全局访问点

角色 单例

优点:
    对唯一实例的受控访问
    单例相当于全局变量，但防止了命名空间被污染
"""




class Singleton:
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super(Singleton, cls).__new__(cls)
        return cls._instance



class Myclass(Singleton):
    def __init__(self, a):
        self.a = a


a = Myclass(10)
print(a)
b = Myclass(20)
print(b)

print(a.a)
print(b.a)

print(id(a), id(b))



