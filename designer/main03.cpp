//
// Created by xuming on 2022/7/10.
//

// 合成复用原则，继承组合优先使用组合

#include <iostream>

// 抽象车
class AbstractCar{
public:
    virtual void run() = 0;
};

class DaZhong: public AbstractCar{
public:
    virtual void run(){
        std::cout << "大众车启动 ..." << std::endl;
    }
};

class TuoLaJi: public AbstractCar{
public:
    virtual void run(){
        std::cout << "拖拉机启动 ..." << std::endl;
    }
};

#if 0
class Person: public TuoLaJi{
public:
    void DongFeng(){
        run();
    }
};
#endif

class Person{
public:
    void setCar(AbstractCar* car){
        this->car = car;
    }

    void DouFeng(){
        this->car->run();
    }
    ~Person(){
        if (this->car!= nullptr){
            delete this->car;
            this->car = nullptr;
        }
    }


public:
    AbstractCar* car;
};

void test02(){
    Person* P = new Person;
    P->setCar(new DaZhong);
    P->DouFeng();


    P->setCar(new TuoLaJi);
    P->DouFeng();
    delete P;
}

int main()
{
    test02();
    return 0;
}
