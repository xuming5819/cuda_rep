//
// Created by xuming on 2022/7/21.
//

#include <iostream>

class AbstractTemplat{
public:
    virtual void BoilWater() = 0;     // 注水2
    virtual void Brew() = 0;          // 冲泡
    virtual void PourInCup() = 0;     //倒入杯中
    virtual void AddSomething() = 0;  // 加入辅料S

    // 模板方法

    void Make(){
        BoilWater();
        Brew();
        PourInCup();
        AddSomething();
    }

};


// 冲泡 caffe

class Coffee: public AbstractTemplat{
public:
    virtual void BoilWater()
    {
        std::cout << "煮山泉水 ..." << std::endl;
    }
    virtual void Brew()
    {
        std::cout <<  "冲泡咖啡 ..." << std::endl;
    }
    virtual void PourInCup(){
        std::cout << "咖啡倒入杯中 ..." << std::endl;

    }
    virtual void AddSomething(){
        std::cout << "加糖 ..." << std::endl;
    }
};

class Tea: public AbstractTemplat{
public:
    virtual void BoilWater()
    {
        std::cout << "煮山泉水 ..." << std::endl;
    }
    virtual void Brew()
    {
        std::cout <<  "冲泡茶叶..." << std::endl;
    }
    virtual void PourInCup(){
        std::cout << "茶叶倒入杯中 ..." << std::endl;

    }
    virtual void AddSomething(){
        std::cout << "加糖 ..." << std::endl;
    }
};

void test01(){
    Tea* tea = new Tea;
    tea->Make();

    Coffee* coffee = new Coffee;
    coffee->Make();
}

int main()
{
    test01();
    return 0;
}

// 子类实现具体的算法, 父类定义好执行顺序