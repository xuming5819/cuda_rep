//
// Created by xuming on 2022/7/17.
//

#include <iostream>



// 提供一种代理来控制对其它对象的访问

class AbstractCommonInterface{
public:
    virtual void run() = 0;
};


// 我已经写好的系统
class MySystem: public AbstractCommonInterface{
public:
    virtual void run()
    {
        std::cout << "系统启动 ......" << std::endl;
    }
};


class MySystemProxy: public AbstractCommonInterface{
public:
    MySystemProxy(std::string username, std::string password) {
        this->m_Username = username;
        this->m_Password = password;
        pSystem = new MySystem;

    }
    ~MySystemProxy()
    {
        if (pSystem != nullptr){
            delete pSystem;
        }
    }
    bool checkUsernameAndPassword(){
        if (m_Username == "admin" && m_Password == "admin123"){
            return true;
        }else{
            return false;
        }
    }

    virtual void run()
    {
        if (checkUsernameAndPassword()){
            std::cout << "用户名和密码正确 ..." << std::endl;
            this->pSystem->run();
        }
    }

public:
    MySystem* pSystem;
    std::string m_Username;
    std::string m_Password;
};

// 必须要有权限验证，不是所有人都能来启动我的启动，提供用户名和密码



void test01()
{
    MySystemProxy* proxy = new MySystemProxy("xuming", "123");
    proxy->run();
}


int main()
{
    test01();
    return 0;
}