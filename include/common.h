//
// Created by work on 2022/5/18.
//

#ifndef CUDA_TEST_COMMON_H
#define CUDA_TEST_COMMON_H

#include <stdio.h>
#include <iostream>
#include <cuda_runtime.h>




//检测GPU
bool CheckCUDA(void){
    int count = 0;
    int i = 0;

    cudaGetDeviceCount(&count);
    if (count == 0) {
        printf("找不到支持CUDA的设备!\n");
        return false;
    }
    cudaDeviceProp prop;
    for (i = 0; i < count; i++) {
        if (cudaGetDeviceProperties(&prop, i) == cudaSuccess) {
            if (prop.major >= 1) {
                break;
            }
        }
    }
    if (i == count) {
        printf("找不到支持CUDA的设备!\n");
        return false;
    }
    cudaGetDeviceProperties(&prop, 0);
    printf("GPU is: %s\n", prop.name);
    cudaSetDevice(0);
    printf("CUDA initialized success.\n");
    return true;
}


static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}

#endif //CUDA_TEST_COMMON_H
